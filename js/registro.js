$(function(){
	var addEducation = $(".btn-add-education"),
		addExperience = $(".btn-add-experience");

	// Agrega mas campos de estudios
	addEducation.on("click", function(event) {
		event.preventDefault();
		duplicateFields($("#section-education"), $(this));
	});

	addExperience.on("click", function(event) {
		event.preventDefault();
		duplicateFields($("#section-experience"), $(this));
	});

	var duplicateFields = function(section, btn) {
		var $sectionClone = section.clone().removeAttr("id"),
			parent = btn.parent();
		parent.before($sectionClone);
	}
});